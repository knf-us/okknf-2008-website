<?php

$trans = array("none" => "", "mgr" => "mgr", "inz" => "inż.", "mgr inz" => "mgr inż.",
			   "dr" => "dr", "dr hab" => "dr hab.", "doc" => "doc.", "prof" => "prof. dr hab.");

?>


<h2>Seminaria</h2>

<ul>

<?php

$q = $dbc->query("SELECT us.degree, us.name, us.surname, ".
				"un.name AS uni, us.desc AS descr, lecture, lecture_title, publication FROM ".
				TBL_USER." us LEFT JOIN unis un ON un.id = us.university ".
				"WHERE degree IN ('none','mgr','inz','mgr inz') && active=1 && lecture_title != '' ".
				"ORDER BY uni, us.name");

$cnt = 0;

while ($t = $q->fetch(PDO::FETCH_ASSOC)) {
	echo '<li><b>'.$t['lecture_title'].'</b>, <i>'.$trans[$t["degree"]]." ".$t["name"]." ".$t["surname"].'</i>, '.$t["uni"].'<br />';
	echo latex_content($t["lecture"]);
	echo '<br />Publikacja: ';
	if ($t["publication"]) {
		echo "tak";
	} else {
		echo "nie";
	}
	echo '<br /><br /></li>';
	$cnt++;
}

echo "</ul><p>Liczba zgłoszonych seminariów: $cnt</p>";

$q = $dbc->query("SELECT COUNT(*) FROM ".TBL_USER." WHERE publication = 1 && active = 1 && (lecture_title != '' || poster_title != '')");
$r = $q->fetch();

echo '<p>Liczba publikacji: '.$r[0].'</p>';

?>

<h2>Postery</h2>

<ul>
<?php

$q = $dbc->query("SELECT us.degree, us.name, us.surname, ".
				"un.name AS uni, us.desc AS descr, poster_title FROM ".
				TBL_USER." us LEFT JOIN unis un ON un.id = us.university ".
				"WHERE degree IN ('none','mgr','inz','mgr inz') && active=1 && poster_title != '' ".
				"ORDER BY uni, us.name");

$cnt = 0;

while ($t = $q->fetch(PDO::FETCH_ASSOC)) {
	echo '<li><b>'.$t['poster_title'].'</b>, <i>'.$trans[$t["degree"]]." ".$t["name"]." ".$t["surname"].'</i>, '.$t["uni"].'</li>';
	$cnt++;
}

echo "</ul><p>Liczba zgłoszonych posterów: $cnt</p>";

?>

<h2>Uwagi</h2>

<ul>
<?php

$q = $dbc->query("SELECT us.degree, us.name, us.surname, ".
				"un.name AS uni, us.desc AS descr, comments FROM ".
				TBL_USER." us LEFT JOIN unis un ON un.id = us.university ".
				"WHERE active=1 && comments != '' ".
				"ORDER BY uni, us.name");

$cnt = 0;

while ($t = $q->fetch(PDO::FETCH_ASSOC)) {
	echo '<li>'.$t['name']." ".$t['surname'].' ('.$t["uni"].'): '.$t['comments'].'</li>';
}

echo "</ul>";

?>

<h2>Nie jem zwierzątek AKA wyżywienie wegetariańskie</h2>

<ul>

<?php
$q = $dbc->query("SELECT us.degree, us.name, us.surname, ".
				"un.name AS uni, us.desc AS descr, comments FROM ".
				TBL_USER." us LEFT JOIN unis un ON un.id = us.university ".
				"WHERE active=1 && food = 'veg' ".
				"ORDER BY uni, us.name");

$cnt = 0;

while ($t = $q->fetch(PDO::FETCH_ASSOC)) {
	echo '<li>'.$t['name']." ".$t['surname'].' ('.$t["uni"].')</li>';
}


?>

</ul>


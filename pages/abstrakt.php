<?php
$q = $dbc->prepare("SELECT name, surname, lecture_title, lecture FROM ".TBL_USER." WHERE login = ? AND lecture_title != '' AND lecture_accepted = 1");
	$q->execute(array($_REQUEST["login"]));
	$r = $q->fetch(PDO::FETCH_ASSOC);
	$q->closeCursor();

	$dir = strtolower(substr($r["name"], 0, 1)).strtolower($r["surname"]);

	echo '<div class="box infobox abstract"><h1>Podgląd abstraktu</h1><h3>'.$r["lecture_title"].'</h3><p>'.latex_content($r["lecture"]).'</p></div>';
	if (file_exists("stuff/$dir")) {
		echo '<p><a href="stuff/'.$dir.'">Materiały z prezentacji konferencyjnej</a></p>';
	}
?>

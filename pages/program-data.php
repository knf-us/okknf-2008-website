<?php
	$program = array("Czwartek, 24. kwietnia 2008 r." =>
		array(
			"08:00-20:00" => "desc:Rejestracja uczestników",
			"13:30-15:00" => "food:Obiad",
			"15:00-16:30" => "desc:Czas wolny",
			"16:30-20:00" => "lect:Uroczyste rozpoczęcie konferencji",
			"17:00" => "aburian",
			"18:00-20:00" => "food:Kolacja",
			),

		"Piątek, 25. kwietnia 2008 r." =>
		array(
			"09:00-10:20" => "food:Śniadanie",
			"10:20-13:30" => "lect:I sesja wykładowa",
			"10:20" => "czarna",
			"10:45" => "spock",
			"11:10" => "wiking",
			"11:35" => "przerwa",
			"11:50" => "Luigiano",
			"12:15" => "perzan",
			"12:40" => array("bandit", "wnusiek"),
			"13:15-14:45" => "food:Obiad",
			"14:45-18:00" => "lect:II sesja wykładowa",
			"14:45" => "diviner",
			"15:10" => "paqel",
			"15:35" => "mireksam",
			"16:00" => "przerwa",
			"16:15" => "Hania_M",
			"16:40" => "cornica",
			"17:05" => "Devona",
			"17:30" => "pbarbarski",
			"18:00-19:00" => "food:Kolacja",
			"19:30-+\\infty" => "desc:Impreza integracyjna",
		),

		"Sobota, 26. kwietnia 2008 r." =>
		array(
			"09:00-10:20" => "food:Śniadanie",
			"10:20-13:30" => "lect:III sesja wykładowa",
			"10:20" => "hczyz",
			"11:05" => "MarcinM",
			"11:30" => "ttylec",
			"11:55" => "przerwa",
			"12:10" => "bszymanik",
			"12:35" => "marpio",
			"13:00" => "michal_krupinski",
			"13:30-15:00" => "food:Obiad",
			"15:00-18:00" => "lect:IV sesja wykładowa",
			"15:00" => "aptok",
			"15:25" => "mrawski",
			"15:50" => "topie",
			"16:15" => "przerwa",
			"16:25" => "justsz",
			"16:50" => "ezapala",
			"17:15" => "sabishi",
			"17:40" => "tolewicz",
			"18:05-19:00" => "food:Kolacja",
			"19:30-+\\infty" => "desc:Impreza integracyjna"
		),

		"Niedziela, 27. kwietnia 2008 r." =>
		array(
			"09:00-10:20" => "food:Śniadanie",
			"10:20-12:15" => "lect:V sesja wykładowa",
			"10:20" => "anuchnabe",
			"10:45" => "Feldmarshall",
			"11:10" => "przerwa",
			"11:25" => "andreas",
			"11:50" => "Bakcyl",
			"12:15-13:30" => "lect:Rozdanie dyplomów",
			"13:30-15:30" => "food:Obiad, uroczyste zakończenie konferencji"
		)
	);
?>

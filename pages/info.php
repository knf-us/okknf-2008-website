<h2>Informacje ogólne</h2>

<ul>

<li>Studentów prosimy o wpłaty:
	<ul>
		<li><b>150 PLN</b> (opłata za nocleg i wyżywienie) na konto:<br />
		<b>02 1140 2004 0000 3802 4843 2067</b>,<br />
		FHU RASZLO Emanuel Olszar<br />
		43-400 Cieszyn, ul. Bielska 66<br />
		z dopiskiem: "OKKNF"</li>
		<li><b>20 PLN</b> (opłata konferencyjna) na konto: <br />
		<b>ING BANK ŚLĄSKI, 74 1050 1214 1000 0007 0000 7909</b>,<br />
		Uniwersytet Śląski<br />
		40-007 Katowice, ul. Bankowa 12<br />
		z dopiskiem: "PIKNIK NAUKOWY 2008"</li>
	</ul>
	<b>UWAGA:</b> Faktury będą wystawiane automatycznie na właściciela rachunku, z którego wykonywany będzie przelew.
</li>

<li>Jedno koło naukowe może na konferencji reprezentować maksymalnie
7 osób, z czego przynajmniej jedna powinna wygłosić wykład.</li>
<li>Zachęcamy również wszystkich do przygotowywania plakatów na sesję posterową.</li>
<li><b>Ostateczny termin rejestracji mija 11. kwietnia 2008!</b></li>
<li>W przypadku chęci wygłoszenia wykładu należy do <b>11. kwietnia 2008</b> podać jego tytuł oraz abstrakt.</li>
<li>Opłata konferencyjna wynosi <b>170 PLN</b> (dla studentów) i <b>270 PLN</b> (dla pracowników naukowych).
</li>
</ul>

<h2>Rejestracja</h2>

<p>Rejestracji na konferencję można dokonać <a href="reg">na tej stronie</a>.
Przy rejestracji należy wypełnić wszystkie wymagane pola formularza rejestracyjnego.
Po zakończeniu procesu rejestracji otrzymasz konto, które pozwoli Ci na określenie
dodatkowych preferencji oraz zmianę niektórych ustawień w dowolnym momencie od
chwili rejestracji do deadline 11. kwietnia.</p>

<p>W przypadku chęci wygłoszenia wykładu lub zaprezentowania posteru naukowego, nie jest
konieczne podawanie jego tytułu i abstraktu w momencie rejestracji. Dane te mogą być
uzupełnione w dowolnym momencie przed 11. kwietnia.</p>

<h2>Wykłady</h2>

<p>Warunkiem koniecznym wygłoszenia wykładu jest podanie jego abstraktu w formularzu
rejestracyjnym.  Abstrakty powinny być napisane w języku polskim.  W abstraktach
można używać wyrażeń LaTeX-owych, np. $\int_{-\infty}^{\infty} e^{-x^2} dx = \sqrt{\pi}$
będzie widoczne jako: <img src="img/latex_sample.gif" style="vertical-align:middle;" alt="the gaussian integral" />.</p>

<p>Prezentacje nie powinny być dłuższe niż 30 min, z czego 5 min powinno być poświęcone
na pytania od publiczności. Na miejscu będzie dostępny projektor oraz komputer z oprogramowaniem
Acrobat Reader 7.0, MS PowerPoint 2000 oraz OpenOffice 2.0.</p>

<p><b>UWAGA</b>: Ze względu na ograniczony czas konferencji, w przypadku dużej liczby zgłoszeń
zastrzegamy sobie możliwość wyboru wykładów, które będą prezentowane podczas Pikniku Naukowego 2008.
Decyzja o przyjęciu bądź odrzuceniu wykładu będzie podjęta przez komitet organizacyjny konferencji
do dnia <b>14. kwietnia 2008 r.</b>  Nie przewidujemy podobnych ograniczeń ilościowych w stosunku do
posterów.</p>

<h2>Zakwaterowanie</h2>

<img src="img/orzel12.jpg" alt="Orzeł Biały" style="float: right; width: 300px; padding-left: 10px; padding-bottom: 20px;" />

<p>Piknik Naukowy 2008 odbędzie się w pensjonacie <a href="http://www.bialy-orzel.pl">Biały Orzeł</a> w Brennej.
Zakwaterowanie na czas konferencji wiąże się z koniecznością uiszczenia
opłaty konferencyjnej w wysokości:</p>
<ul>
	<li>170 PLN (studenci)</li>
	<li>270 PLN (pracownicy naukowi)</li>
</ul>

<p>W opłatę konferencyjną wliczony jest koszt noclegu w pensjonacie, wyżywienia (3 posiłki dziennie,
wg programu konferencji) oraz materiałów konferencyjnych.  Dostępne będą pokoje 2, 3, i 4-osobowe.
Każdy pokój wyposażony jest w pełen węzeł sanitarny.</p>


<h2>Dane kontaktowe</h2>

<img src="img/orzel01.jpg" alt="Orzeł Biały" style="float: left; width: 300px; padding-right: 10px; padding-bottom: 20px;" />
<p>
<b>Pensjonat "BIAŁY ORZEŁ"</b><br />
43 - 438 Brenna<br />
ul. Leśników 30<br />
tel. 033 858 73 57<br />
tel./fax: 033 858 73 58<br />
</p>

<br style="clear: both;" />


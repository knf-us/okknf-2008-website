<?php
	require_once("common.php");
	theme_header();
?>
<div id="main">
	<div class="hshadow"></div>
	<div id="rightcontent">
		<p style="text-align: center;"><img src="img/logo.png" alt="logo Pikniku Naukowego 2008" /></p>
		<h1>Ważne terminy:</h1>
		<ul>
			<li><b>11 kwiecień 2008</b><br /> <span style="color:red; font-weight:bold;">deadline rejestracji</span></li>
			<li><b>11 kwiecień 2008</b><br /> <span style="color:red; font-weight:bold;">deadline zgłaszania tytułów i abstraktów wykładów</span></li>
			<li><b>24 kwiecień 2008</b><br /> rozpoczęcie <span class="nobr">Pikniku Naukowego 2008</span></li>
			<li><b>27 kwiecień 2008</b><br /> zakończenie <span class="nobr">Pikniku Naukowego 2008</span></li>
		</ul>

		<h1>Patronat:</h1>
		<ul>
<!--			<li><b>prof. dr hab. Janusz Janeczek</b><br />JM Rektor Uniwersytetu Śląskiego w Katowicach</li>
			<li><b>prof. dr hab. Reinhard Kulessa</b><br />Prezes Zarządu Głównego Polskiego Towarzystwa Fizycznego</li>
-->
			<li><b>p. Iwona Szarek</b><br />Wójt gminy Brenna</li>
			<li><b>prof. dr hab. Krystian Roleder</b><br />Dyrektor Instytutu Fizyki UŚ</li>
		</ul>

		<h1>Sponsorzy:</h1>
		<ul>
			<li><a href="http://www.us.edu.pl/">Uniwersytet Śląski</a></li>
			<li><a href="http://www.euroart.com.pl/">EuroArt</a></li>
			<li><a href="http://www.tamtron.com.pl/"><img src="img/tamtron.png" style="vertical-align:middle;" alt="Logo firmy Tamtron" /></a></li>
		</ul>
	</div>
	<div id="maincontent">
<?php
	$page = get_req_page();

	if (file_exists("pages/$page.php")) {
		include("pages/$page.php");
		$lastmod = filemtime("pages/$page.php");
	} else {
		echo <<<HTML
<div class="box msgbox"><h1>Błąd</h1>
<p>Nie znaleziono strony.</p>
</div>
HTML;
		$lastmod = time();
	}
?>
	</div>
</div>
<?php
	theme_footer();
?>

